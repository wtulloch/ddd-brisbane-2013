﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace TestHarness
{
    class DemoServiceTester
    {
        private static Uri personnelEndpointHttps = new Uri("https://localhost:44309/api/personnel/");
        private static Uri personnelEndpointHttp = new Uri("http://localhost:53867/api/personnel/");

        static void Main(string[] args)
        {
            Console.WriteLine("Calling Personnel Service");

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(personnelEndpointHttp).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var results = response.Content.ReadAsStringAsync().Result;
                    DisplayEmployees(results);

                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    Console.WriteLine("Unauthorised");
                }
            }

				Console.ReadLine();
        }
        private static void DisplayEmployees(string results)
        {
            var serializer = new JavaScriptSerializer();
            var employees = serializer.Deserialize<List<Employee>>(results);
            foreach (var employee in employees)
            {
                Console.WriteLine(" ({0}) {1} {2}: {3}", employee.Id, employee.FirstName, employee.LastName, employee.Email);
            }

        }
      

       
    }
     public class Employee
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Username { get; set; }
            public string Email { get; set; }
        }
}
