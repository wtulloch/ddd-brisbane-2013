﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DemoService.Models;

namespace DemoService.Controllers
{
    public class PersonnelController : ApiController
    {
        private readonly IDataService<EmployeeDto> _dataService;

        public PersonnelController(IDataService<EmployeeDto> dataService )
        {
            _dataService = dataService;
        }

        public HttpResponseMessage Get()
        {
            var employees = _dataService.GetAll();

            var response = Request.CreateResponse(HttpStatusCode.OK, employees);
            
            return response;
        }

        public HttpResponseMessage Get(int id)
        {
            var employee = _dataService.Get(id);
            if (employee == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Employee not found");
            }

            return Request.CreateResponse(HttpStatusCode.OK, employee);
        }
    }
}
