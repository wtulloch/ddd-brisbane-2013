﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace DemoService.Models
{
    public class EmployeeDataService : IDataService<EmployeeDto>
    {
        private EmployeeContext _dbContext;

        public EmployeeDataService()
        {
            _dbContext = new EmployeeContext();
        }
        public List<EmployeeDto> GetAll()
        {
            var employees = new List<EmployeeDto>();
            try
            {
                var employeeQuery = _dbContext.Employees
                    .Include("EmployeeRoles")
                    .ToList();

                employees.AddRange(employeeQuery.Select(ConvertToDto));
            }
            catch (Exception ex)
            {

               Debugger.Break();
            }
            return employees;
        }

        public EmployeeDto Get(int id)
        {
            var employee = _dbContext.Employees.FirstOrDefault(e => e.Id == id);
            
            if (employee == null)
            {
                throw new ArgumentException("employee could not be found");
            }

            var dto = ConvertToDto(employee);

            return dto;
        }

        public EmployeeDto Add(EmployeeDto item)
        {
            //Not needed for demos
            return null;
        }

        public EmployeeDto Update(EmployeeDto item)
        {
           //not needed for demos
            return null;
        }

        public void Delete(int id)
        {
           //not needed from demos
        }

        private static EmployeeDto ConvertToDto(Employee employee)
        {
            if (employee == null)
                return null;

            var employDto = new EmployeeDto
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Username = employee.Username,
                Email = employee.Email
            };
            foreach (var employeeRole in employee.EmployeeRoles)
            {
                employDto.Roles.Add(employeeRole.Role.Name);
            }
            return employDto;
        }
    }
}