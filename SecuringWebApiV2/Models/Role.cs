﻿using System.Collections.Generic;

namespace DemoService.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<EmployeeRole> EmployeeRoles { get; set; }
    }
}